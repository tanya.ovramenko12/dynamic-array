﻿#include <string>
#include <iostream>

class DynamicArray
{
public:
    DynamicArray() //конструктор без параметров
    {
        size = 0;
        capacity = 0;
        arr = new int[0];
    }
    DynamicArray(int n) //конструктор с одним параметром
    {
        capacity = n;
        size = 0;
        arr = new int[n];
    }
    void add(int val) //добавление элемента в массив
    {
        if (size == capacity)
        {
            resize(size);
        }
        arr[size] = val;
        size++;
    }
    void add(int val, int i) //добавление элемента в массив по индексу
    {
        if (size < i || i < 0)
        {
            throw std::exception("Index is out of range!");
        }
        if (size == 0 && capacity == 0)
        {
            resize(1);
        }
        else if (size == capacity)
        {
            resize(size);
        }
        for (int k = size; k > i; k--)
        {
            arr[k] = arr[k - 1];
        }
        arr[i] = val;
        size++;
        print();
    }
    void resize(int x) //изменение вместимости массива
    {
        int* arr1 = new int[size + x];
        for (int i = 0; i < size + x; i++)
        {
            arr1[i] = arr[i];
        }
        delete arr;
        arr = arr1;
        capacity += x;
    }
    void pop(int i) //удаление элемента по индексу
    {
        if (i >= size || i < 0)
        {
            throw std::exception("Index is out of range!");
        }
        for (int k = i + 1; k < size; k++)
        {
            arr[k - 1] = arr[k];
        }
        size--;
    }
    void pop() //удаление элемента с конца
    {
        if (size == 0)
        {
            throw std::exception("Array is empty!");
        }
        size--;
    }
    void print() //вывод элементов массива
    {
        if (size == 0)
        {
            throw std::exception("Array is empty.");
        }
        for (int i = 0; i < size; i++)
        {
            std::cout << arr[i] << " ";
        }
        std::cout << std::endl;
    }
    int& at(int i) // возвращает значение элемента массива по индексу с возможностью изменения
    {
        if (i < 0 || i > size)
        {
            throw std::exception("Index is out of range!");
        }
        return arr[i];
    }
private:
    int* arr;
    int size;
    int capacity;
};

int main()
{
    std::cout << "Dynamic Arrays: " << "1. create empty array" << std::endl << "2. create array with capacity" << std::endl <<
        "3. add value at the end" << std::endl << "4. add value at index" << std::endl << "5. delete value at the end" << std::endl <<
        "6. delete value at index" << std::endl << "7. value at index" << std::endl << "8. end programme" << std::endl << "9. print array" << std::endl;
    DynamicArray* arr;
    std::string str;
    getline(std::cin, str);
    if (str == "create array with capacity")
    {
        int n;
        std::cin >> n;
        arr = new DynamicArray(n);
    }
    else
    {
        arr = new DynamicArray();
    }
    do
    {
        getline(std::cin, str);
        if (str == "add value at the end")
        {
            int val;
            std::cin >> val;
            arr->add(val);
        }
        else if (str == "add value at index")
        {
            int val;
            int i;
            std::cin >> val >> i;
            try
            {
                arr->add(val, i);
            }
            catch (std::exception & ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "delete value at the end")
        {
            try
            {
                arr->pop();
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "delete value at index")
        {
            int i;
            std::cin >> i;
            try
            {
                arr->pop(i);
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "value at index")
        {
            int i;
            std::cin >> i;
            try
            {
                std::cout << arr->at(i);
                std::cout << std::endl;
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "print array")
        {
            try
            {
                arr->print();
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
    }     while (str != "end programme");
}